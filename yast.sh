# remove old scripts from game dir
rm -r /home/docnight/'Gothic II'/_work/Data/Scripts/Content
rm -r /home/docnight/'Gothic II'/_work/Data/Scripts/System

# now - copy a new content
cp -a ./Scripts/* /home/docnight/'Gothic II'/_work/Data/Scripts/

# compile scripts
wine /home/docnight/'Gothic II'/System/Gothic2.exe -zreparse
